const log = console.log
const error = console.error
const tableHead = document.querySelector('#thead')
const tableBody = document.querySelector('#table')

log('Hello multiverse')


const usdFormatter = new Intl.NumberFormat('en-us', { // formatting our currency to whole dollar amounts
    style: 'currency',
    currency: 'USD',
})

const addCommaFormatter = new Intl.NumberFormat()




fetch('data.json') // fetching data from our json file that is serving as our temporary db
    .then((response) => {
        if (response.ok) { // some error handling todo: refactor to use try/catch instead and see if it is more readable
            return response.json() //TODO: add some error handling in response 
        } else {
            return Promise.reject(response)
        }
    })
    .then((obj) => {
        log('Response from Nomics API', obj)
            //TODO: render data to the DOM in tabular fashion 
        const renderData = () => {
            obj.forEach((coin) => { // for each coin create a table row with table data 

                let tr = document.createElement('tr')
                const tableTemplate = (table) => {
                    table = `<tr id="table-row">
                <td class="table-data">
                <div class="currency-logo">
               ${coin.rank} 
                </div>
                </td>
                 <td class="table-data"><div class="currency-logo">${coin.name}</td>
                 <td class="table-data"><div class="currency-logo">${usdFormatter.format(coin.price)}</div></td>
                 <td class="table-data"><div class="currency-logo">${coin['1d'].price_change_pct} % </div></td>
                 <td class="table-data"><div class="currency-logo">${usdFormatter.format(coin.market_cap)}</div></td>
                 <td class="table-data"><div class="currency-logo">${usdFormatter.format(coin['30d'].volume)}</div></td>
                <td class="table-data"><div class="currency-logo">${addCommaFormatter.format(coin.circulating_supply)}</div></td> 
                </tr>`
                    return table
                }
                tr.innerHTML = tableTemplate()
                tableBody.appendChild(tr)
                log(`${coin.name} ${coin.market_cap} ${coin.price}`)
            })
        }
        return renderData()
    })
    .catch((err) => {
        error('Something went wrong', err)
    })
    //todo: download each crypto coin logo and include it in an assets folder

const templateValue = [
        { price: 'PRICE' },
        { oneDayChange: '1D CHANGE' },
        { marketCap: 'MARKET CAP' },
        { volume: 'VOLUME' }
    ]
    //TODO: finsih installing and configuring tailwind css(if it takes more than 25 minutes just use the CDN during dev untill we figure it out )
    //todo: make fetch call to dtatbase url and retrieve latest data and append to the screen