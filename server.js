const fs = require('fs');
const path = require('path')
const mongo = require('mongodb').MongoClient
const mongoURL = 'mongodb://localhost:27017'
const mongoose = require('mongoose')
const schema = mongoose.Schema

const db = require('./db')
const cryptoCurrency = require('./models/cryptoCurrency.js')
const nomicsAPI = require('./nomics')

const express = require('express')



const log = console.log
const error = console.error


const app = express()
const router = express.Router()
const port = 3000

// Serving all static files in the public folder
app.use(express.static('public'))

//serving our application 
app.listen(port, () => {
    log(`Listening on port: ${port}!`)
})