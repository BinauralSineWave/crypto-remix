const fs = require('fs');
const path = require('path')

const db = require('./db')
    // helper modules
const express = require('express')
const mongo = require('mongodb').MongoClient
const mongoURL = 'mongodb://localhost:27017'
const axios = require('axios')
const request = require('postman-request')
const fetch = require('node-fetch')

const CoinMarketCap = require('coinmarketcap-api')

//files
const server = require('./server')
const _db = require('./db')

const log = console.log
const error = console.error


const apiKey = '11809352-062f-4ac7-8ae5-8fc91f695071'
const URL = "https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest?start=1&limit=5000&convert=USD"



const client = new CoinMarketCap(apiKey)




const NOMICS_KEY = '8d6d783f836ba7978b8a72c04f45b1744fec01e3'

const NOMICS_URL = `https://api.nomics.com/v1/currencies/ticker?key=${NOMICS_KEY}&interval=1d,30d&convert=USD&per-page=100&page=1`

// const GET_NOMICS = request({ url: NOMICS_URL, json: true }, (err, response, body) => {
//     error('error:', err)
//     log('statusCode:', response && response.statusCode)
//     log('body:', body)

//     const cryptoJSON = JSON.stringify(body, null, 2)
//     fs.writeFileSync('crypto-data.json', cryptoJSON) // Writing body (data) to json file
//     log('Looking inside response', cryptoJSON)

// })

const GET_NOMICS_CLIENT = request({ url: NOMICS_URL, json: true }, (err, response, body) => { //Nomics API free tier only lets you make 1 API call at a time so this code fails alternatively but there are no errors its a matter of Nomics policy the call below is a temporary place holder untill we build out the controller routes and data base
    error('error:', err)
    log('statusCode:', response && response.statusCode)
    log('body:', body)

    const cryptoJSON = JSON.stringify(body, null, 2)
    fs.writeFileSync('public/data.json', cryptoJSON) // Writing body (data) to json file
    log('Looking inside response', cryptoJSON)

})


module.exports = {
        // GET_NOMICS: GET_NOMICS,
        GET_NOMICS_CLIENT: GET_NOMICS_CLIENT,
        axios: axios,
        request: request,
        fetch: fetch,
        log: log,
        error: error
    }
    //TODO How write JSON to the db
    //TODO: figure out how to route data from the server to the front end
    //Todo: figure out how to implement MVC architecture pattern