const mongoose = require('mongoose')
const Schema = mongoose.Schema

const log = console.log
const error = console.error
    // const bitCoin = new Schema({
    //     name: { type: String, required: true },
    //     price: { type: Number, required: true }

// })

const altCoinSchema = new Schema({
        id: { type: String, required: true },
        currency: { type: String, required: true },
        symbol: { type: Number, required: true },
        name: { type: String, required: true },
        logo_url: { type: String, required: true },
        status: { type: String, required: true },
        price: { type: Number, required: true },
        price_date: { type: Number, required: true },
        price_timestamp: { type: Number, required: true },
        circulating_supply: { type: Number, required: true },
        max_supply: { type: Number, required: true },
        market_cap: { type: Number, required: true },
        market_cap_dominance: { type: Number, required: true },
        num_exchanges: { type: Number, required: true },
        num_pairs: { type: Number, required: true },
        num_pairs_unmapped: { type: Number, required: true },
        first_candle: { type: Number, required: true },
        first_trade: { type: Number, required: true },
        first_order_book: { type: Number, required: true },
        rank: { type: Number, required: true },
        rank_delta: { type: Number, required: true },
        high: { type: Number, required: true },
        high_timestamp: { type: Number, required: true }

    })
    // write out db schemas for bitCoin and altCoins
    //TODO: How to get JSON from API call into db and mapped to a schema

const oneDayChange = new Schema({
    volume: { type: Number, required: true },
    price_change: { type: Number, required: true },
    price_change_pct: { type: Number, required: true },
    volume_change: { type: Number, required: true },
    volume_change_pct: { type: Number, required: true },
    market_cap_change: { type: Number, required: true },
    market_cap_change_pct: { type: Number, required: true },
})

const thirtyDayChange = new Schema({
    volume: { type: Number, required: true },
    price_change: { type: Number, required: true },
    price_change_pct: { type: Number, required: true },
    volume_change: { type: Number, required: true },
    volume_change_pct: { type: Number, required: true },
    market_cap_change: { type: Number, required: true },
    market_cap_change_pct: { type: Number, required: true },
})

const cryptoCoinSchema = mongoose.model(' Alt Coin and  Currency Price Change Data', altCoinSchema, oneDayChange, thirtyDayChange)
const bitCoinSchema = mongoose.model('Bit Coin', altCoinSchema)


module.exports = {
    cryptoCoinSchema: cryptoCoinSchema,
    bitCoinSchema: bitCoinSchema
}

log('Im in the crypto data base', bitCoinSchema, cryptoCoinSchema)